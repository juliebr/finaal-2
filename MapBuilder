import java.util.ArrayList;
import java.util.Random;

public class MapBuilder {
	
	private int height;
	private int width;
	private int maxMoves;
	private int maxNumOfBoxes;
	private int reqNumOfBoxes;
	
	public Map newMap(Difficulty currDifficulty, GameType currGameType) {
		Map currMap = null;
		setMapParameters(currDifficulty);
		if (currGameType == GameType.SINGLE) {
			while (currMap == null) {
				currMap = generateSingleMap();
			} 
			return currMap;
		} else {
			while (currMap == null) {
				currMap = generateMultiPlayerMap();
			} 
			return currMap;
		}

	}
	
	public void setMapParameters(Difficulty currDifficulty) {
		if(currDifficulty == Difficulty.EASY) {
	        height = 7;
	        width = 7;
	        maxMoves = 40;
	        maxNumOfBoxes = 10;
			reqNumOfBoxes = 3;
		} else if (currDifficulty == Difficulty.MEDIUM) {
	        height = 8;
	        width = 8;
	        maxMoves = 60;
	        maxNumOfBoxes = 20;
			reqNumOfBoxes = 4;
		} else if (currDifficulty == Difficulty.HARD) {
	        height = 10;
	        width = 10;
	        maxMoves = 80;
	        maxNumOfBoxes = 30;
			reqNumOfBoxes = 6;
		}
		
	}
	
	public Map generateSingleMap() {   
		        
        Random randGen = new Random();
        Map currMap = new Map(height, width);
    	currMap.setUpMap();
    	

    	int startPlayerRow = randGen.nextInt(height-2) + 1;
    	int startPlayerCol = randGen.nextInt(width-2) + 1;
    	currMap.setPlayer(startPlayerRow, startPlayerCol);
    	MapPosition startingPlayerPosition = new MapPosition(startPlayerRow, startPlayerCol);
    	
    	
    	//Randomly picks the time at which the boxes will be placed
        ArrayList<Integer> stepOfPlacement = new ArrayList<Integer>();
        for(int i = 0; i < maxNumOfBoxes; i++) {
        	int step = randGen.nextInt(maxMoves/2);
        	if(!stepOfPlacement.contains(step)) stepOfPlacement.add(step);
        }
        
        //Walk Randomly and drop boxes infront of players
        ArrayList<MapPosition> playerVisited = new ArrayList<MapPosition>();
        ArrayList<MapPosition> boxLocations = new ArrayList<MapPosition>();
        playerVisited.add(startingPlayerPosition);
        ArrayList<Direction> direction = new ArrayList<Direction>();
        direction.add(Direction.DOWN);
        direction.add(Direction.UP);
        direction.add(Direction.LEFT);
        direction.add(Direction.RIGHT);
        
        for(int i = 0; i < maxMoves; i++) {
        	int pos = randGen.nextInt(4);
        	MapPosition nextPosition = nextMoveSingle(currMap, direction.get(pos));
        	//currMap.printMap();
        	if(currMap.getTileType(nextPosition.getRow(), nextPosition.getColumn()) == TileType.EMPTY && stepOfPlacement.contains(i) && !boxLocations.contains(nextPosition) 
        			&& !playerVisited.contains(nextPosition)) {
        		currMap.setBox(nextPosition.getRow(),nextPosition.getColumn());
        		boxLocations.add(nextPosition);
        	}
        	playerVisited.add(nextPosition);
        	currMap.movePlayer(direction.get(pos));
        }
        
        
        //turn current player Location to an empty location
        currMap.setEmpty(currMap.getRowPlayer(), currMap.getColPlayer());
        
       //Common between multi and single!
		for(int row = 0; row < currMap.getHeight(); row++) {
			for(int col = 0; col < currMap.getWidth(); col++) {
				MapPosition currPos = new MapPosition(row,col);
				if(currMap.getTileType(row, col) == TileType.BOX) {
					currMap.setGoal(row, col);
				} else if (!playerVisited.contains(currPos) && !boxLocations.contains(currPos)) {
					currMap.setWall(row, col);
				}
			}
		}
        
		//Place the player in its starting position
		if (currMap.getTileType(startPlayerRow, startPlayerCol) == TileType.EMPTY) {
			currMap.setPlayer(startPlayerRow, startPlayerCol);
		} else if (currMap.getTileType(startPlayerRow, startPlayerCol) == TileType.GOAL){
			currMap.setGoalPlayer(startPlayerRow, startPlayerCol);
		}
		
		
		//Common between multi and single!
		//place the boxes in their inital positions
		//if you place a box ontop of a goal, place a goal box instead
		for(MapPosition mapPos : boxLocations) {
			if(currMap.getTileType(mapPos.getRow(), mapPos.getColumn()) == TileType.EMPTY) {
				currMap.setBox(mapPos.getRow(), mapPos.getColumn());
			} else if (currMap.getTileType(mapPos.getRow(), mapPos.getColumn()) == TileType.GOAL) {
				//currMap.setGoalBox(mapPos.getRow(), mapPos.getColumn());
				currMap.setEmpty(mapPos.getRow(), mapPos.getColumn());
				//DECIDE ON WEATHER TO DO THIS OR NOT
			}
		}
        
		int numOfNormalBoxes = 0;
        //then we check if there are enough boxes that arent on goals in the game as some boxes dont move
		for(int row = 0; row < currMap.getHeight(); row++) {
			for(int col = 0; col < currMap.getWidth(); col++) {
				if (currMap.getTileType(row, col) == TileType.BOX) numOfNormalBoxes++;
			}
		}
				
		if(numOfNormalBoxes >= reqNumOfBoxes) {
			return currMap;
		} else {
			return null;
		}
		
	}
	
	public MultiPlayerMap generateMultiPlayerMap() {
        Random randGen = new Random();
        MultiPlayerMap currMap = new MultiPlayerMap(height, width);
    	currMap.setUpMap();
		
    	//initialise starting position
    	int startPlayerRow = randGen.nextInt(height-2) + 1;
    	int startPlayerCol = randGen.nextInt(width-2) + 1;
    	currMap.setPlayer(startPlayerRow, startPlayerCol);
    	MapPosition startPlayerPosition = new MapPosition(startPlayerRow, startPlayerCol);
    	MapPosition startPlayerTwoPosition = new MapPosition(startPlayerRow, startPlayerCol);
    	while(startPlayerTwoPosition.equals(startPlayerPosition) ) {
    		startPlayerTwoPosition.setRow(randGen.nextInt(height-2) + 1);
    		startPlayerTwoPosition.setColumn(randGen.nextInt(width-2) + 1);
    	}
		currMap.setPlayerTwo(startPlayerTwoPosition.getRow(), startPlayerTwoPosition.getColumn());
    	
    	//Randomly picks the time at which the boxes will be placed
        ArrayList<Integer> stepOfPlacement = new ArrayList<Integer>();
        for(int i = 0; i < maxNumOfBoxes; i++) {
        	int step = randGen.nextInt(maxMoves/2);
        	if(!stepOfPlacement.contains(step)) stepOfPlacement.add(step);
        }
        
        //Walk Randomly and drop boxes infront of players
        ArrayList<MapPosition> bothPlayerVisited = new ArrayList<MapPosition>();
        ArrayList<MapPosition> boxLocations = new ArrayList<MapPosition>();
        bothPlayerVisited.add(startPlayerPosition);
        bothPlayerVisited.add(startPlayerTwoPosition);
        ArrayList<Direction> direction = new ArrayList<Direction>();
        direction.add(Direction.DOWN);
        direction.add(Direction.UP);
        direction.add(Direction.LEFT);
        direction.add(Direction.RIGHT);
    	
       for(int i = 0; i < maxMoves; i++) {
        	int pos = randGen.nextInt(4);
        	Boolean movePlayerTwo = false;
        	int whoseMove = randGen.nextInt(2);
        	if (whoseMove == 1) {
        		movePlayerTwo = true;
        	}
        	MapPosition nextPosition = nextMoveMulti(currMap, direction.get(pos), movePlayerTwo);
        	//currMap.printMap();
        	if(currMap.getTileType(nextPosition.getRow(), nextPosition.getColumn()) == TileType.EMPTY && stepOfPlacement.contains(i) && !boxLocations.contains(nextPosition)
        			&& !bothPlayerVisited.contains(nextPosition)) {
        		currMap.setBox(nextPosition.getRow(),nextPosition.getColumn());
        		boxLocations.add(nextPosition);
        	}
        	bothPlayerVisited.add(nextPosition);
        	if (movePlayerTwo) {
        		currMap.movePlayerTwo(direction.get(pos));
        	} else {
            	currMap.movePlayer(direction.get(pos));
        	}
        }
  
        //turn current player Location to an empty location
        currMap.setEmpty(currMap.getRowPlayer(), currMap.getRowPlayer());
        currMap.setEmpty(currMap.getRowPlayerTwo(), currMap.getColPlayerTwo());
        
        for(int row = 0; row < currMap.getHeight(); row++) {
			for(int col = 0; col < currMap.getWidth(); col++) {
				MapPosition currPos = new MapPosition(row,col);
				if(currMap.getTileType(row, col) == TileType.BOX) {
					currMap.setGoal(row, col);
				} else if (!bothPlayerVisited.contains(currPos) && !boxLocations.contains(currPos)) {
					currMap.setWall(row, col);
				}
			}
		}
        
		//Place the player in its starting position
		if (currMap.getTileType(startPlayerRow, startPlayerCol) == TileType.EMPTY) {
			currMap.setPlayer(startPlayerRow, startPlayerCol);
		} else if (currMap.getTileType(startPlayerRow, startPlayerCol) == TileType.GOAL){
			currMap.setGoalPlayer(startPlayerRow, startPlayerCol);
		}
        
		if (currMap.getTileType(startPlayerTwoPosition.getRow(), startPlayerTwoPosition.getColumn()) == TileType.EMPTY) {
			currMap.setPlayerTwo(startPlayerTwoPosition.getRow(), startPlayerTwoPosition.getColumn());	
		} else if (currMap.getTileType(startPlayerTwoPosition.getRow(), startPlayerTwoPosition.getColumn()) == TileType.GOAL) {
			currMap.setGoalPlayerTwo(startPlayerTwoPosition.getRow(), startPlayerTwoPosition.getColumn());	
		}
		
		
		//place the boxes in their inital positions
		//if you place a box ontop of a goal, place a goal box instead
		//Boxes wont spawn on you, therefore you dont have to search for goalPlayer
		for(MapPosition mapPos : boxLocations) {
			if(currMap.getTileType(mapPos.getRow(), mapPos.getColumn()) == TileType.EMPTY) {
				currMap.setBox(mapPos.getRow(), mapPos.getColumn());
			} else if (currMap.getTileType(mapPos.getRow(), mapPos.getColumn()) == TileType.GOAL) {
				//currMap.setGoalBox(mapPos.getRow(), mapPos.getColumn());
				currMap.setEmpty(mapPos.getRow(), mapPos.getColumn());
				//DECIDE ON WEATHER TO DO THIS OR NOT
			}
		}
        
		int numOfNormalBoxes = 0;
        //then we check if there are enough boxes that arent on goals in the game as some boxes dont move
		for(int row = 0; row < currMap.getHeight(); row++) {
			for(int col = 0; col < currMap.getWidth(); col++) {
				if (currMap.getTileType(row, col) == TileType.BOX) numOfNormalBoxes++;
			}
		}
        
		if(numOfNormalBoxes >= reqNumOfBoxes) {
			return currMap;
		} else {
			return null;
		}
    	
	}
		
	public MapPosition nextMoveSingle(Map currMap, Direction movement) {
		Map temp = currMap.copyMap();
		temp.movePlayer(movement);
    	MapPosition newPosition = new MapPosition(temp.getRowPlayer(), temp.getColPlayer());
    	return newPosition;
	}
	
	public MapPosition nextMoveMulti(MultiPlayerMap currMap, Direction movement, Boolean movePlayerTwo) {
		MultiPlayerMap temp = currMap.copyMap();
		MapPosition newPosition;
		if (movePlayerTwo) {
			temp.movePlayerTwo(movement);
	    	newPosition = new MapPosition(temp.getRowPlayerTwo(), temp.getColPlayerTwo());
		} else {
			temp.movePlayer(movement);
	    	newPosition = new MapPosition(temp.getRowPlayer(), temp.getColPlayer());
		}
    	return newPosition;
	}
	
	

}
